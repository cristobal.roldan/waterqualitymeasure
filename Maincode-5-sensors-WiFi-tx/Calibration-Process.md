#Procesos de calibración

Cambiar el flag "EnableCalibration" en la definición: 1 para PH | 2 para EC | 0 para deshabilitar

Para asegurar una precisión correcta en la calibración y en general ara el proceso de medición, **se debe incorporar un sensor de temperatura** para realizar una compensación automática.
Luego activar las líneas de calibración correspondientes a cada sensor.
Luego se deben enviar los comandos por medio de la consola serial

##Sensor de PH

Serial Commands:
* enterph -> enter the calibration mode
* calph   -> calibrate with the standard buffer solution, two buffer solutions(4.0 and 7.0) will be automaticlly recognized
* exitph  -> save the calibrated parameters and exit from calibration mode


##Sensor EC
Analog Electrical Conductivity Sensor / Meter Kit V2 (K=1.0).

 Serial Commands:
 *   enterec -> enter the calibration mode
 *   calec -> calibrate with the standard buffer solution, two buffer solutions(1413us/cm and 12.88ms/cm) will be automaticlly recognized
 *   exitec -> save the calibrated parameters and exit from calibration mode

##Sensor TDS

Calibration CMD:
* enter -> enter the calibration mode
* cal:[tds value] -> calibrate with the known tds value(25^c). e.g. cal:707
* exit -> save the parameters and exit the calibration mode

