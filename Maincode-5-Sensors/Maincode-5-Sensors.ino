/*
* @file  : Maincode
* @author: Cristóbal Roldán
* @date  : 27/10/2021
* @brief : Código para el control de sensores destinados 
*          a medición de la calidad del agua. 
*/

/*----------Begin Includes----------------*/
#include <OneWire.h>               // Sensor de temperatura 
#include <DallasTemperature.h>     //Sensor de temperatura
#include <EEPROM.h>                // Para la calibración de todos los sensores
#include "DFRobot_PH.h"            // Sensor de Ph
#include "DFRobot_EC.h"            // Sensor de EC
#include "GravityTDS.h"            // Sensor de TDS



/*----------end Includes----------------*/

/*---------- Begin definde and local Variables ----------------*/
#define TurbidityPin A4
#define temp_pin 7
#define PH_PIN A5
#define EC_PIN A2
#define TdsSensorPin A3
#define EnableCalibration 0 // 1 para PH | 2 para EC | 0 para deshabilitar

#define TDSAdDress 20   // Dirección de memoria donde se almacena la K del Td #importante
#define center 0.444

float Turbidityval,VoltTurbi; // Turbidity
float temperature = 20 ;      // Temp
float phValue,phVolt;         // PH
float tdsVolt2,tdsValue2;     // TDS por medio de EC.
float ecVoltage,ecValue;      // EC
float tdsValue = 0;           // TDS


/*---------- End  definde and local Variables ----------------*/

/*------------ Declaraciónes -------------------------------------*/
OneWire ourWire(temp_pin);                //Se establece el temp_pin como bus OneWire (Sensor temperatura)
DallasTemperature tempsensor(&ourWire); //Se declara una variable u objeto para nuestro sensor  (Sensor temperatura)
DFRobot_PH ph;          // Se establece el sensor de ph
DFRobot_EC ec;          // Se establece el sensor de ec.
GravityTDS gravityTds;  //Se establece el sensor TDS
/*-------------- End Declaraciónes ------------------------------*/

/*---------- Begin Prototipe funcion ----------------*/
void getTurbidity();  // mide y entrega el valor en pantalla de turbidez
void getPH();  // mide y entrega el valor en pantalla de PH
void getTemp(); // Medición de temperatura
void getEC(); // Medición de conductividad eléctrica
void getTDS(); // Medición de TDS

void tds_begin(); // Inicialización del sensor TDS

/*---------- End Prototipe funcion ----------------*/


/*---------- Begin Code ----------------*/
void setup(){
  pinMode(13, OUTPUT); //Led indicador
  Serial.begin(9600); // Iniciamos la comunicación serial
  tempsensor.begin();   //Se inicia el sensor de temperatura
  ph.begin();          // Se inicia el sensor de PH 
  ec.begin();          // Se inicia el sensor de EC
  tds_begin();          // Se inicia el sensor TDS  
 
}
void loop(){

    static unsigned long timepoint = millis();
    if(millis()-timepoint>2000U){                  //time interval: 3s
        timepoint = millis();
        
        getTemp();  // Obtención de valores de todos los sensores
        getTurbidity();
        getPH(); 
        getEC();     
        //getTDS();  // Se obtiene a través de la medición de PH
        Serial.println("-------------------------------------------------");
        
      }


} // final setup
/*---------- End code ----------------*/



/*------------------Begin Funcions --------------------*/

void getTurbidity(){
   // Se propone calibrar el sensor mediante una linealización cerca del punto de operación,
   // Calibrándolo por medio del sensor de TDS o Conductividad.
   VoltTurbi =((analogRead(TurbidityPin)/1024.0)*5.0)-center; // Voltaje leído
   Serial.print("turbidity Volt: ");
   Serial.print(VoltTurbi,3);
   Turbidityval =(-1120.4*(VoltTurbi)*(VoltTurbi))+(5742.3*(VoltTurbi))-4352.9 ;
   Serial.print("  Trubidity value: ");
   Serial.println( Turbidityval);
   
}

void getPH(){
  
        phVolt = analogRead(PH_PIN)/1024.0*5000;  // read the voltage
        phValue = ph.readPH(phVolt,temperature);  // convert voltage to pH with temperature compensation
        Serial.print("pH:");
        Serial.println(phValue,2);
        //Serial.print(" PHvolt:");  Serial.println(phVolt);  // debug mediante voltaje
        if (EnableCalibration == 1){
            ph.calibration(phVolt,temperature);   // ACTIVAR PARA CALIBRAR by Serail CMD - Pasos en el readme
        }
     
}

void getTemp(){
  
   tempsensor.requestTemperatures();   //Se envía el comando para leer la temperatura
   temperature = tempsensor.getTempCByIndex(0); //Se obtiene la temperatura en ºC
   Serial.print("Temperatura= ");
   Serial.print(temperature);
   Serial.println(" C");  
   
}


void getEC(){

    ecVoltage = analogRead(EC_PIN)/1024.0*5000;   // read the voltage
    ecValue =  ec.readEC(ecVoltage,temperature);  // convert voltage to EC with temperature compensation
    tdsValue2 = (1000*ecValue)/2;  // TDS medición FACTOR 0,5
    Serial.print("EC:");
    Serial.print(ecValue,4);
    Serial.println("ms/cm");
    Serial.print(tdsValue2,0);
    Serial.println(" ppm");
    
    if (EnableCalibration==2){
        ec.calibration(ecVoltage,temperature);   // ACTIVAR PARA CALIBRAR by Serail CMD - Pasos en el readme
    }
}


void getTDS(){
    gravityTds.setTemperature(temperature);  // set the temperature and execute temperature compensation
    gravityTds.update();  //sample and calculate
    tdsValue = gravityTds.getTdsValue();  // then get the value
    Serial.print(tdsValue,1);
    Serial.println(" ppm");
    if (EnableCalibration==1){
      gravityTds.ONcalibrate();   // Función creada por C.R para habilitar y deshabilitar la calibración (fue modificada la librería)
                                       // ACTIVAR PARA CALIBRAR by Serail CMD - Pasos en el readme
    }
}


void tds_begin(){
    gravityTds.setPin(TdsSensorPin);
    gravityTds.setAref(5.0);  //reference voltage on ADC, default 5.0V on Arduino UNO
    gravityTds.setAdcRange(1024);  //1024 for 10bit ADC;4096 for 12bit ADC
    gravityTds.setKvalueAddress(TDSAdDress); // Dirección de memoria para guardar la constante K de calibración.
    gravityTds.begin();  //initialization
}
/*------------------End Funcions --------------------*/
