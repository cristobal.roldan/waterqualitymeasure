# WaterQualityMeasure

Este código integra la medición de los 5 sensores:
* PH
* EC
* TDS
* TEMP
* Turbidity.


Estos se deben calibrar en forma periódica y **tener las precauciónes** descritas en el archivo "Warnings" adjunto.
Por su parte las instrucciónes de calibración se encuentran en el archivo "Calibration-Process".

