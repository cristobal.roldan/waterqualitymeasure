# WaterQualityMeasure

Medición de PH y turbidez del agua.

INDICACIÓNES
    Se debe calibrar el sensor mediante el código de calibración, el cuál se haya en la biblioteca "DFRobot_PH-master".
    Las instrucciónes de calibración se hayan en: https://wiki.dfrobot.com/Gravity__Analog_pH_Sensor_Meter_Kit_V2_SKU_SEN0161-V2

<p>DIAGRAMA DE CONECCIÓN SENSOR DE PH:</p>
<img src="Others/img/ph-conection.png" width="348" height="254"/>

<p>DIAGRAMA DE CONECCIÓN SENSOR DE TURBIDEZ.</p>

<div>
<img src="Others/img/turbidity-conection.png" width="270" height="245"/>
</div>

<p>CURVA CARACTERÍSTICA SENSOR DE TURBIDEZ.</p>
<p>NTU VS VOLTAJE</p>
<img src="Others/img/graf-turbidity.png" width="250" height="235"/>

