﻿#WaterQualityMeasure

Medición de la calidad del agua, para catastro de las condiciones marinas, posterior comunicación con LoRa

#Temp-2.
Código para testeo del sensor de temperatura DS18B20
Ocupa únicamente las librerías OneWire y DallasTemperature.
Esta última ahorra las líneas de código en el Main, relacionadas a la lectura bit por bit de los datos entregados por el sensor, además de la generación de un buffer.



Resistencia de 4.7 [KΩ]
