# WaterQualityMeasure

Medición de la calidad del agua, para catastro de las condiciones marinas, posterior comunicación con LoRa

## IMPORTANTE:

### SENSOR DE PH
  - El conector BNC y la placa de conversión deben mantenerse completamente secos.
  - La burbuja de vidrio de la sonda debe **EVITAR TOCAR CUALQUIER MATERIAL DURO**
  cualquier daño o raya hará que el sensor falle.
  - La sonda no debe conectarse prolongadamente a la placa de conversión sin la fuente de alimentación
  - Lavar la sonda con agua destilada, evitar la contaminación cruzada.
  - Cuando la sonda se usa por primera vez o se usa durante algún tiempo la sonda debe sumergirse en la solución 3NKCL durante 8 horas.

### SENSOR DE TURBIDEZ
  -**La parte superior de la sonda no es impermeable.**
  -Posee una salida digital opcional mediante switch, HIGH/LOW con umbral
  ajustable vía potenciómetro.

### Sensor TDS
  -No sobrepasar los 55° C. 
  -**La sonda no debe estar demasiado cerca de los bordes del contenedor**, esto podría afectar la lectura.


### Sensor de conductividad eléctrica
  La sonda es una sonda de laboratorio. 
  **No la sumerja en líquido durante mucho tiempo**. De lo contrario, esto acortará la vida útil de la sonda.
  **La capa de negro platino** se adhiere a la superficie de la chapa de la sonda. **Debe evitar cualquier objeto Solo se puede lavar con agua destilada, de lo contrario, la capa de negro platino se dañará**, lo que resultará en una medición inexacta.



## Características importantes

### Sensor de PH
  Vin (3.3 - 5.5V)  / Vout (0 - 3.0 V) / Precisión +- 0.1 
  Tiempo de respuesta < 2 min / Temp 5 - 60°

### Sensor de Turbidez
  Vinput (5V cc)  / Vout (0-4.5 V)  /  tiempo Respuesta < 0.5 S.
  temp 5 - 90°  / **PRECISIÓN +- 30 NTU**

### Sensor TDS
  Vinput (3.3 - 5V cc)  / Vout (0-2.3 V) / 0 - 1000 ppm
  **temp max 50°**  / PRECISIÓN 10% (FS 25°C)

### Sensor conductividad éléctrica.
  Vinput (3.0 - 5V cc)  / Vout (0- 3.4 V) / 0 - 1000 ppm
  **temp max 40°**  / PRECISIÓN 5% (FS 25°C)
  **VIDA UTIL > 0.5 AÑOS**